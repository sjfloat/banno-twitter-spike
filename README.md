# Banno Twitter Fest

## Credentials

It will be necessary to supply your Twitter Developer Credentials. We use the
[circe-config](https://github.com/circe/circe-config) library.


You may either edit the existing

    src/main/resources/reference.conf

or supply your own

    src/main/resources/application.conf

which overrides `reference.conf`

## Running

If things work as expected, you will be able to simply execute the `run` script, which will build
the required jar file the first time it is executed. Of course that will not be necessary for
subsequent executions.

## Areas to Improve

Because I wanted to exhibit the app's streaminess, the results are written directly to the console
in real-time (or as real-time as your environment actually permits, of course). It should be easy to
incorporate an HTTP server stream into the mix to expose the JSON encoded results either discretely
or continuously, similarly to the way it does to STDOUT now. The working state (which is handled
purely) would be available to such an endpoint.

There are portions of the tweet processing itself that could be executed independently and therefore
concurrently, in separate workers. Without investigation, it is unclear how much this would improved
throughput or liveness.

Clearly, the tracking of domains, hashtags, and emojis in memory is an eventually losing
proposition. I believe this is the only non-constant memory space in the app. I've run this for
several minutes without issues, but eventually memory will be exhausted. In a more realistic
version, it would be necessary to persist these data.

I normally type things very strongly and validate very strictly. But in this case, the Twitter
stream is The Source of Truth and performance was prioritized somewhat over correctness. I did
employ some value types after the fact. I would like to run some kind of benchmarks to see what that
cost us in terms of time performance.

