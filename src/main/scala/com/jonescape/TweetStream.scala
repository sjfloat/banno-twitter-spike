
package com.jonescape

import com.jonescape.Processor.process

import cats.effect._
import cats.effect.concurrent.Ref
import fs2.io.stdout
import fs2.Stream
import fs2.text.utf8Encode
import io.circe.config.parser
import io.circe.generic.auto._
import io.circe.Json
import java.time.Instant
import java.util.concurrent.Executors
import jawnfs2._
import org.http4s._
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.oauth1
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global

class TweetStream[F[_]](implicit F: ConcurrentEffect[F], cs: ContextShift[F]) {

  implicit val f = io.circe.jawn.CirceSupportParser.facade

  def accRefStream(zero: Instant): Stream[F,Ref[F,CummulativeTweetStats]] =
    Stream.eval(Ref.of[F, CummulativeTweetStats](CummulativeTweetStats(zero,zero)))

  case class AppSettings(creds: Credentials)
  case class Credentials(consumerKey: String, consumerSecret: String, accessToken: String, accessSecret: String)

  def sign(creds: Credentials)(req: Request[F]): F[Request[F]] = {
    val consumer = oauth1.Consumer(creds.consumerKey, creds.consumerSecret)
    val token    = oauth1.Token(creds.accessToken, creds.accessSecret)
    oauth1.signRequest(req, consumer, callback = None, verifier = None, token = Some(token))
  }

  def jsonStream(creds: Credentials)(req: Request[F]): Stream[F, Json] =
    for {
      client <- BlazeClientBuilder(global).stream
      sr  <- Stream.eval(sign(creds)(req))
      res <- client.stream(sr).flatMap(_.body.chunks.parseJsonStream)
    } yield res

  def stream(creds: Credentials, acc: Ref[F,CummulativeTweetStats])(blockingEC: ExecutionContext): Stream[F, Unit] = {
    val req = Request[F](Method.GET, Uri.uri("https://stream.twitter.com/1.1/statuses/sample.json"))
    val s: Stream[F, Json] = jsonStream(creds)(req)
    s.evalMap(j => process(j, acc)).map(_.display).through(utf8Encode).through(stdout(Blocker.liftExecutionContext(blockingEC)))
  }

  def blockingEcStream: Stream[F, ExecutionContext] =
    Stream.bracket(F.delay(Executors.newFixedThreadPool(4)))(pool =>
        F.delay(pool.shutdown()))
      .map(ExecutionContext.fromExecutorService)

  val getCreds: Stream[F, Credentials] = Stream.eval(parser.decodePathF[F, Credentials]("application.creds"))

  def run: F[Unit] =
    (
      for {
        creds <- getCreds
        blockingEc <- blockingEcStream
        zero <-Stream.eval(F.delay(Instant.now))
        acc <- accRefStream(zero)
        strm <- stream(creds, acc)(blockingEc)
      } yield strm
    ).compile.drain

}

