package com.jonescape

import com.jonescape.domain._

case class TweetStats(
  timestamp: java.time.Instant,
  domains: Set[Domain],
  hashTags: Set[Hashtag],
  emojis: Set[Emoji],
  hasPhotoUrl: Boolean,
  hasUrl: Boolean
)
