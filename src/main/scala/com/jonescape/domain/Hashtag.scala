package com.jonescape.domain

case class Hashtag(value: String) extends AnyVal { override def toString = value }
