package com.jonescape.domain

case class Url(value: String) extends AnyVal { override def toString = value }
