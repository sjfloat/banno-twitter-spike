package com.jonescape.domain

case class Emoji(value: String) extends AnyVal { override def toString = value }
