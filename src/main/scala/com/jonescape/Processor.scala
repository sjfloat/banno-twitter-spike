package com.jonescape

import analytics.Analytics.registerTweet
import com.jonescape.analytics.Analytics
import com.jonescape.analytics.Analytics.analyze
import Extractor.{extractTweet, extractStats}

import cats.effect._
import cats.effect.concurrent.Ref
import cats.implicits._
import io.circe.Json

object Processor {

  def process[F[_]: Sync](json: Json, acc: Ref[F,CummulativeTweetStats]): F[Analytics] =
    for {
      tweet <- extractTweet(json)
      stats <- Sync[F].delay(extractStats(tweet))
      _ <- registerTweet(stats, acc)
      cum <- acc.get
      analytics <- Sync[F].delay(analyze(cum))
    } yield analytics

}
