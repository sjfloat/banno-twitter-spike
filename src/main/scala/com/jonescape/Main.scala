package com.jonescape

import cats.effect.{ExitCode, IO, IOApp}

object Main extends IOApp {
  def run(args: List[String]) =
    (new TweetStream[IO]).run.as(ExitCode.Success)
}
