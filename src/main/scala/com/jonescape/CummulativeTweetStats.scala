package com.jonescape

import com.jonescape.domain._

import java.time.Instant

case class CummulativeTweetStats(
  startTime: Instant,
  timestamp: Instant,
  totalNumber: Long,
  domains: Map[Domain, Long],
  hashTags: Map[Hashtag, Long],
  emojis: Map[Emoji, Long],
  numberWithPhotoUrls: Long,
  numberWithUrls: Long
)

object CummulativeTweetStats {
  def apply(startTime: Instant, timestamp: Instant) = new CummulativeTweetStats(
    startTime,
    timestamp,
    0,
    Map(),
    Map(),
    Map(),
    0,
    0
  )
}


