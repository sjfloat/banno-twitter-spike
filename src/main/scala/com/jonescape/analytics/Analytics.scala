package com.jonescape.analytics

import java.time.Duration

import cats.effect.Sync
import cats.effect.concurrent.Ref
import cats.implicits._

import com.jonescape.CummulativeTweetStats
import com.jonescape.TweetStats
import com.jonescape.domain.{Domain, Emoji, Hashtag}

case class Analytics(
  totalNumber: Long,
  durationSeconds: Long,
  topDomains: List[Domain],
  topHashTags: List[Hashtag],
  topEmojis: List[Emoji],
  numberWithEmojis: Long,
  percentWithEmojis: Float,
  percentWithUrls: Float,
  percentWithPhotoUrls: Float,
  averagePerSecond: Float,
  averagePerMinute: Float,
  averagePerHour: Float
) {

  // TODO Implement Show
  def display: String = {
    s"""
\u001b[2J
Number of Tweets:          ${totalNumber}
Time period (in seconds):  ${durationSeconds}
Number with Emojis:        ${numberWithEmojis}
Percent with Emojis:       ${percentWithEmojis}%
Percent with Urls:         ${percentWithUrls}%
Percent with Photo Urls:   ${percentWithPhotoUrls}%
Average tweets per second: ${averagePerSecond}
Average tweets per minute: ${averagePerMinute}
Average tweets per hour:   ${averagePerHour}
""" +
    s"\nTop Domains:\n  ${topDomains.mkString("\n  ")}\n" +
    s"\nTop Hashtags:\n  ${topHashTags.mkString("\n  ")}\n" +
    s"\nTop Emojis:\n  ${topEmojis.mkString("\n  ")}\n"


  }

}

object Analytics {
  def getNumberWithEmojis(c: CummulativeTweetStats): Long = c.emojis.size.toLong
  def getTopDomains(c: CummulativeTweetStats): List[Domain] = rankMap(c.domains)
  def getTopHashtags(c: CummulativeTweetStats): List[Hashtag] = rankMap(c.hashTags)
  def getTopEmojis(c: CummulativeTweetStats): List[Emoji] =  rankMap(c.emojis)

  def getPercentWithEmojis(c: CummulativeTweetStats): Float =
    (getNumberWithEmojis(c).toFloat / c.totalNumber) * 100

  def getPercentWithUrls(c: CummulativeTweetStats): Float =
    (c.numberWithUrls.toFloat / c.totalNumber) * 100

  def getPercentWithPhotoUrls(c: CummulativeTweetStats): Float =
    (c.numberWithPhotoUrls.toFloat / c.totalNumber) * 100

  def getAverages(c: CummulativeTweetStats): (Float,Float,Float) = {
    val perSecond = c.totalNumber.toFloat / durationSeconds(c)
    val perMinute = perSecond * 60
    (perSecond, perMinute, perMinute * 60)
  }

  def durationSeconds(c: CummulativeTweetStats): Long = Duration.between(c.startTime, c.timestamp).getSeconds

  def analyze(c: CummulativeTweetStats): Analytics = {
    val (perSecond, perMinute, perHour) = getAverages(c)
    Analytics(
      c.totalNumber,
      durationSeconds(c),
      getTopDomains(c),
      getTopHashtags(c),
      getTopEmojis(c),
      getNumberWithEmojis(c),
      getPercentWithEmojis(c),
      getPercentWithUrls(c),
      getPercentWithPhotoUrls(c),
      perSecond, perMinute, perHour
    )
  }

  // TODO This could use some stream processing.
  def rankMap[A](m: Map[A, Long]): List[A] =
    m.toSeq.sortWith(_._2 > _._2).map(_._1).toList.take(5)

  def registerTweet[F[_]: Sync](n: TweetStats, acc: Ref[F,CummulativeTweetStats]): F[Unit] = {
    for {
      c <- acc.get
      cum <- Sync[F].delay{
        new CummulativeTweetStats(
          c.startTime,
          n.timestamp,
          c.totalNumber + 1,
          maybeIncrement(n.domains, c.domains),
          maybeIncrement(n.hashTags, c.hashTags),
          maybeIncrement(n.emojis, c.emojis),
          c.numberWithPhotoUrls + (if (n.hasPhotoUrl) 1 else 0),
          c.numberWithUrls + (if (n.hasUrl) 1 else 0)
        )
      }
      _ <- acc.set(cum)
    } yield ()

  }

  def maybeIncrement[A](s: Set[A], m: Map[A, Long]): Map[A, Long] = {
    s.foldLeft(m){ (acc, k) => 
      if (acc contains k)
        acc + (k -> (acc(k) + 1))
      else
        acc + (k -> 1)
    }
  }

}

