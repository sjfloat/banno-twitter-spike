package com.jonescape

import com.jonescape.domain._

import cats.effect._
import io.circe.generic.auto._
import io.circe.Json

object Extractor {

  sealed trait TweetOrDelete
  case object Delete extends TweetOrDelete

  case class UrlElm(expanded_url: String)
  case class HashtagElm(text: String)
  case class Media(
    `type`: String, // Should probably be more strongly typed (excuse pun)
    expanded_url: String
  )
  case class ExtendedEntities(media: List[Media])
  case class Entities(
    hashtags: List[HashtagElm],
    urls: List[UrlElm],
    media: Option[List[Media]]
  )
  case class ExtendedTweet(
    full_text: String,
    entities: Entities
  )
  case class Tweet (
    text: String,
    entities: Entities,
    extended_tweet: Option[ExtendedTweet],
    extended_entities: Option[ExtendedEntities]
  ) extends TweetOrDelete

  def extractTweet[F[_]: Sync](json: Json): F[TweetOrDelete] =
    Sync[F].delay(
        json.as[Tweet] match {
          case Right(tweet) => {
            tweet
          }
          case Left(err) => {
            if (err.getMessage == "Attempt to decode value on failed cursor: DownField(text)")
              Delete
            else
              // Not really recoverable
              throw(err)
          }
        }
      )

  def extractStats(tweetOrDelete: TweetOrDelete): TweetStats = {
    // TODO effectful time
    val timestamp = java.time.Instant.now

    tweetOrDelete match {
      case tweet: Tweet => {

        def allEntities = getAllEntities(tweet)
        def allMedia = getAllMedia(tweet)
        def urls = getUrls(allEntities)

        TweetStats(
          timestamp,
          getDomains(urls),
          getHashTags(allEntities),
          getEmojis(tweet),
          hasPhotoUrl(allMedia),
          (! urls.isEmpty)
        )

      }
      case Delete => TweetStats(timestamp, Set(), Set(), Set(), false, false)
    }
  }

  def getUrls(entities: List[Entities]): Set[Url] =
    entities.flatMap(_.urls.map(_.expanded_url)).map(Url).toSet

  def getHashTags(entities: List[Entities]): Set[Hashtag] =
    entities.flatMap(_.hashtags.map(_.text)).map(Hashtag).toSet

  def getAllEntities(tweet: Tweet): List[Entities] =
    List(tweet.entities) ++ (
      tweet.extended_tweet match {
        case Some(et) => List(et.entities)
        case None => Nil
      }
    )

  def getAllMedia(tweet: Tweet): List[Media] =
    (
      tweet.extended_entities match {
        case Some(ee) => ee.media
        case None => Nil
      }
    ) ++ tweet.entities.media.getOrElse(Nil)


  def getEmojis(tweet: Tweet): Set[Emoji] = {
    val text = tweet.text

    // Lifted from twemoji
    val pattern = "((([\uD83C\uDF00-\uD83D\uDDFF]|[\uD83D\uDE00-\uD83D\uDE4F]|[\uD83D\uDE80-\uD83D\uDEFF]|[\u2600-\u26FF]|[\u2700-\u27BF])[\\x{1F3FB}-\\x{1F3FF}]?))".r

    pattern.findAllIn(text).map(Emoji).toSet
  }

  def hasPhotoUrl(media: List[Media]): Boolean = media.exists(_.`type` == "photo")

  def getDomains(urls: Set[Url]): Set[Domain] =
    urls.map(u => Domain((new java.net.URL(u.value)).getHost))
}
