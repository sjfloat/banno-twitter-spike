
import com.jonescape.analytics.Analytics
import com.jonescape.analytics.Analytics.{analyze, rankMap, registerTweet}
import com.jonescape.CummulativeTweetStats
import com.jonescape.domain.{Domain, Emoji, Hashtag}
import com.jonescape.TweetStats

import cats.effect.concurrent.Ref
import cats.effect.IO
import java.time.Instant
import java.time.temporal.ChronoUnit
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must._

class Test extends AnyFunSuite with Matchers {

  /*
   * Probably wouldn't do this in production code, but for tests...
   */
  implicit def str2Domain(s: String) = Domain(s)
  implicit def str2Emoji(s: String) = Emoji(s)
  implicit def str2Hashtag(s: String) = Hashtag(s)

  val now = Instant.now // arbitrary now
  val secondsAgo = (s: Long) => now.minus(s, ChronoUnit.SECONDS)

  val t1 = TweetStats(
    secondsAgo(10),
    Set("d1", "d2"),
    Set(),
    Set("e1"),
    false, true
  )

  val t2 = TweetStats(
    secondsAgo(5),
    Set("d3", "d2"),
    Set("h1", "h2"),
    Set("e1"),
    true, true
  )

  // Initial state
  val cRef = Ref.of[IO, CummulativeTweetStats](CummulativeTweetStats(secondsAgo(100),secondsAgo(100)))

  test ("nominal registerTweet 1") {
    for {
      ref <- cRef
      _ <- registerTweet(t1, ref)
      _ <- ref.get
    } yield (a: CummulativeTweetStats) => {
      a.timestamp must be (secondsAgo(10))
      a.totalNumber must be (1)
      a.domains must be (Map("d1" -> 1, "d2" -> 1))
      a.hashTags must be (Map())
      a.emojis must be (Map("e1" -> 1))
      a.numberWithPhotoUrls must be (0)
      a.numberWithUrls must be (1)
    }
  }

  test ("nominal registerTweet 2") {
    for {
      ref <- cRef
      _ <- registerTweet(t1, ref)
      _ <- registerTweet(t2, ref)
      _ <- ref.get
    } yield (a: CummulativeTweetStats) => {
      a.timestamp must be (secondsAgo(5))
      a.totalNumber must be (2)
      a.domains must be (Map("d1" -> 1, "d2" -> 2, "d3" -> 1))
      a.hashTags must be (Map("h1" -> 1, "h2" -> 1))
      a.emojis must be (Map("e1" -> 2))
      a.numberWithPhotoUrls must be (1)
      a.numberWithUrls must be (2)
    }
  }

  test ("spot-check analyze") {
    val totalNumber = 2
    val durationSeconds = 100-5

    for {
      ref <- cRef
      _ <- registerTweet(t1, ref)
      _ <- registerTweet(t2, ref)
      _ <- ref.get
    } yield (c: CummulativeTweetStats) => {
      val a: Analytics = analyze(c)
      a.totalNumber must be (totalNumber)
      a.durationSeconds must be (durationSeconds)
      a.topDomains.take(1) must be (List("d2"))
      round(a.averagePerSecond.toDouble) must be (round(totalNumber.toFloat.toDouble/durationSeconds))
      println(a.display)
    }
  }

  def round(f: Double): Double = ((f*100).round).toDouble/100

  test("nominal rankMap 1") {
    val m1 = Map[String,Long](
      "a" -> 3,
      "b" -> 30,
      "c" -> 10,
      "d" -> 25,
      "e" -> 35,
      "f" -> 5,
    )

    rankMap(m1) must be (List("e", "b", "d", "c", "f"))
  }

}

