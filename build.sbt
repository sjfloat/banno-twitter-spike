import Dependencies._

ThisBuild / scalaVersion     := "2.13.4"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val catsEffectVersion = "2.5.1"
lazy val http4sVersion = "0.21.24"
lazy val circeVersion = "0.14.1"
lazy val circeConfigVersion = "0.8.0"

lazy val root = (project in file("."))
  .settings(
    name := "twitter-spike",

    // https://github.com/typelevel/cats-effect
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect"         % catsEffectVersion,
      "org.http4s"    %% "http4s-blaze-client" % http4sVersion,
      "org.http4s"    %% "http4s-circe"        % http4sVersion,
      "io.circe"      %% "circe-generic"       % circeVersion,
      "io.circe"      %% "circe-optics"        % circeVersion, // Maybe unneeded later
      "io.circe"      %% "circe-config"        % circeConfigVersion,
      "org.scalatest" %% "scalatest"           % "3.2.9"             % "test",
    ),

    // https://github.com/cb372/scala-typed-holes 
    addCompilerPlugin("com.github.cb372" % "scala-typed-holes" % "0.1.6" cross CrossVersion.full),

    // https://github.com/oleg-py/better-monadic-for
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0"),

    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-unchecked",
      "-explaintypes",
      "-language:postfixOps",
      "-language:higherKinds",
      "-Ywarn-dead-code",
      "-Ywarn-unused:imports",
    ),

    javaOptions += "-Xss512M",

    mainClass in (Compile, packageBin) := Some("com.jonescape.Main")

  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
